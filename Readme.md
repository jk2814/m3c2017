### Introduction to High Performance Computing
#### Autumn, 2017

#### Announcements (see [course webpage](https://imperialhpsc.bitbucket.io) for further information)

14/12/17: There were a few announcements made during lecture 21, please have a look at the first few slides
in lecture21.pdf, note that this lecture will not be on Panopto.

13/12/17: Part3.3: The call to mpe_decomp1d can use size(y)/2 instead of size(y), see p32v4.f90

12/12/17: One more comment on the project has been posted (part3.3)

11/12/17: Two further clarifications to the project have been added online

8/12/17: Clarifications to the project have been added online

A debugged template file, _p32v2.f90_ has been added to the course repo

Solutions for lab 9 and hw3 are available online


4/12/17: Added corrected p12.f90 template file for project .or. --> .and. in bdglobal_mpi, line 39

1/12/17: The final project is now online, see the course webpage and the project/ directory

30/11/17: If you are using MLC machines and the $ which ffmpeg is not working, run $ chmod a+x ffmpeg in /home/userid/bin

29/11/17: Homework 2 solutions have been posted online, marking is in-progress and will require a few more days for completion

28/11/17: Comments/guidance on matplotlib animations have been added to the homework 3 assignment online

A clarifying note on the solution for HW1, question 3 ("time" corresponds to iterations) has been added noline

Be aware that copying and pasting text from slides into your terminal may not work as the terminal may not interpret the pasted characters as you would expect.

25/11/17: Notes on installing _ffmpeg_ have been posted on the course webage

20/11/17: Homework 3 has been posted on the course webpage

15/11/17: HW1 marks for Math students are available on Blackboard

15/11/17: HW1 solutions have been posted on the course webpage

14/11/17: A correction on the convergence criteria for HW2 has been posted online

9/11/17: A correction has been posted online for HW2, question 3

If you are using Xubuntu (or similar), you may need to use "f2py3" instead of "f2py"

9/11/17: Please fill out the very short (anonymous) course feedback form [here](https://goo.gl/forms/FVBLoJgc6C1BnxN43)

7/11/17: A correction has been posted on HW2 online on the convergence criteria used in question 2

6/11/17: Homework 2 has been posted on the course webpage

30/10/17: A note on Homework 1, question 2 has been added online: When a node stops its random walk at Y=0, it should
be considered part of the network.

30/10/17: There will no longer be a Wednesday 11-12 lab session.

25/10/17: Corrections and clarifications have been added to Homework 1 online

23/10/17: Several references have been added to the supplementary material section of the course webpage

23/10/17: Homework 1 has been posted on the course webpage.

19/10/17: Several updates have been made to the course webpage and repo: the final brownian motion code, *brown.py*
including the solution to lab 2, task 2 is available in the *lecture5* folder of the repo; solutions for lab 2 task 1
is in the *lab2* folder, and solutions to the 3rd intro python exercise are in the *python* folder. A useful reference for
finding Numpy routines has been addied to the webpage.

14/10/17:
Modified and finalized details of assessment for the class have been posted

13/10/17: 
If you are having trouble using *pip install* during software installation, just use *pip3 install* instead.

Solutions to the first two python exercises have been posted. Please
remember to work through the 3rd and 4th videos/slides before lecture 4 on Monday.

Codes, slides, exercises and solutions are included in the course repo. Syncing your
fork and then updating your clone (using *git pull*) will give you your own copy
of all of these files.

